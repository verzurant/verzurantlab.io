---
title: "Verzurant Hugo Theme"
date: 2021-05-11
description: A fork of the Hugo Tania theme
weight: 1
link: https://gitlab.com/verzurant/verzurant
repo: https://gitlab.com/verzurant/verzurant
icon: 📝
---